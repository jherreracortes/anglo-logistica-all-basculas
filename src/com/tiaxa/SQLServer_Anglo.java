package com.tiaxa;

import static com.tiaxa.Main.fecha_log;
import java.util.ArrayList;
import java.util.HashMap; 

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLServer_Anglo {

    public static ArrayList<HashMap<String, String>> getData(String url_coneccion, Integer numero_bascula, Integer max_id) {
        String connectionUrl = url_coneccion;
        ArrayList<HashMap<String, String>> data = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            String SQL = "SELECT "
                   + "DISTINCT PESAJE.PesBseCod AS 'BASCULA', "
                    + "PESAJEDOCUMENTOCAMPOS.PDcCamVal AS 'LOTE', "
                    + "PESAJEDOCUMENTO.PDcDocFec AS 'FECHA', "
                    + "PESAJE.PesComFec AS 'FECHA HORA', "
                    + "PESAJEDOCUMENTO.PDcDocNro AS 'GUIA', "
                    + "PESAJE.VhcPat AS 'PATENTE', "
                    + "TRANSPORTISTA.TraNom AS 'TRANSPORTISTA',"
                    + "COMUNA.ComNom AS 'DESTINO', PRODUCTO.ProNom AS 'PRODUCTO',"
                    + "PESAJE.PesVhcComBru AS 'BRUTO (KG)',"
                    + "PESAJE.PesTotTar AS 'TARA (KG)', PESAJE.PesVhcComNet AS 'NETO (KG)',"
                    + "PESAJE.PesNro AS 'N° PESAJE',  "
                    + "PESAJEDOCUMENTO.PDcEst AS 'ESTADO GUIA' "
                    + "FROM EtruckDTE.dbo.CICLOPESAJE CICLOPESAJE, "
                    + "EtruckDTE.dbo.CLIENTE CLIENTE, EtruckDTE.dbo.CLIENTESUCURSAL CLIENTESUCURSAL,"
                    + "EtruckDTE.dbo.COMUNA COMUNA, EtruckDTE.dbo.PESAJE PESAJE, "
                    + "EtruckDTE.dbo.PESAJEDOCUMENTO PESAJEDOCUMENTO, "
                    + "EtruckDTE.dbo.PESAJEDOCUMENTOCAMPOS PESAJEDOCUMENTOCAMPOS, "
                    + "EtruckDTE.dbo.PESAJEDOCUMENTODETALLE PESAJEDOCUMENTODETALLE, "
                    + "EtruckDTE.dbo.PRODUCTO PRODUCTO, "
                    + "EtruckDTE.dbo.TRANSPORTISTA TRANSPORTISTA, "
                    + "EtruckDTE.dbo.CHOFER CHOFER "
                    + "WHERE COMUNA.ComCod = CLIENTESUCURSAL.CliSucComCod "
                    + "AND CLIENTESUCURSAL.CliID = CLIENTE.CliID "
                    + "AND PESAJEDOCUMENTO.PDcDstCliSucCod = CLIENTESUCURSAL.CliSucCod "
                    + "AND CLIENTE.CliID = PESAJEDOCUMENTO.PDcDstCliID "
                    + "AND PESAJEDOCUMENTO.PesBseCod = PESAJEDOCUMENTODETALLE.PesBseCod "
                    + "AND PESAJEDOCUMENTO.PesNro = PESAJEDOCUMENTODETALLE.PesNro "
                    + "AND PESAJEDOCUMENTO.PDcID = PESAJEDOCUMENTODETALLE.PDcID "
                    + "AND PESAJEDOCUMENTODETALLE.PesBseCod = PESAJEDOCUMENTOCAMPOS.PesBseCod "
                    + "AND PESAJEDOCUMENTODETALLE.PesNro = PESAJEDOCUMENTOCAMPOS.PesNro "
                    + "AND PESAJEDOCUMENTODETALLE.PDcID = PESAJEDOCUMENTOCAMPOS.PDcID "
                    + "AND PESAJEDOCUMENTO.PesNro = PESAJE.PesNro AND PESAJE.PesBseCod = PESAJEDOCUMENTOCAMPOS.PesBseCod "
                    + "AND PESAJE.PesCicPesCod = CICLOPESAJE.CicPesCod AND PESAJE.PesTraID = TRANSPORTISTA.TraID "
                    + "AND PESAJEDOCUMENTODETALLE.PDcProCod = PRODUCTO.ProCod AND ((PESAJEDOCUMENTOCAMPOS.PDcCamCod='CLOTE') "
                    + "AND PESAJE.PesBseCod=" + numero_bascula + " and PESAJE.PesNro >=" + max_id + " );";
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL);

            System.out.println("[" + fecha_log() + "]===INICIO RESCATE DATOS SQL SERVER POR MAX NUMERO PESAJE=====");
            while (rs.next()) {
                HashMap<String, String> row = new HashMap<>();
                row.put("bascula", rs.getString(1).trim());
                row.put("lote", rs.getString(2).trim());
                row.put("fecha", rs.getString(3).trim());
                row.put("fecha_hora", rs.getString(4).trim());
                row.put("guia", rs.getString(5).trim());
                row.put("patente", rs.getString(6).trim());
                row.put("transportista", rs.getString(7).trim());
                row.put("destino", rs.getString(8).trim());
                row.put("producto", rs.getString(9).trim());
                row.put("bruto", rs.getString(10).trim());
                row.put("tara", rs.getString(11).trim());
                row.put("neto", rs.getString(12).trim());
                row.put("numero_pesaje", rs.getString(13).trim());
                row.put("rut_chofer", "sin dato");
                row.put("nombre_chofer", "sin dato ");
                row.put("estado_guia", rs.getString(14).trim());
                
                data.add(row);
            }
            System.out.println("[" + fecha_log() + "]===TERMINO RESCATE DATOS SQL SERVER POR MAX NUMERO PESAJE======");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
        return data;

    }
    
    public static ArrayList<HashMap<String, String>> getDataUpdate(String url_coneccion, Integer numero_bascula) {

        String connectionUrl = url_coneccion;
        ArrayList<HashMap<String, String>> data = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            String SQL = "SELECT "
                    + "DISTINCT PESAJE.PesBseCod AS 'BASCULA', "
                    + "PESAJEDOCUMENTOCAMPOS.PDcCamVal AS 'LOTE', "
                    + "PESAJEDOCUMENTO.PDcDocFec AS 'FECHA', "
                    + "PESAJE.PesComFec AS 'FECHA HORA', "
                    + "PESAJEDOCUMENTO.PDcDocNro AS 'GUIA', "
                    + "PESAJE.VhcPat AS 'PATENTE', "
                    + "TRANSPORTISTA.TraNom AS 'TRANSPORTISTA',"
                    + "COMUNA.ComNom AS 'DESTINO', PRODUCTO.ProNom AS 'PRODUCTO',"
                    + "PESAJE.PesVhcComBru AS 'BRUTO (KG)',"
                    + "PESAJE.PesTotTar AS 'TARA (KG)', PESAJE.PesVhcComNet AS 'NETO (KG)',"
                    + "PESAJE.PesNro AS 'N° PESAJE', "
                    + "PESAJEDOCUMENTO.PDcEst AS 'ESTADO GUIA' " 
                    + "FROM EtruckDTE.dbo.CICLOPESAJE CICLOPESAJE, "
                    + "EtruckDTE.dbo.CLIENTE CLIENTE, EtruckDTE.dbo.CLIENTESUCURSAL CLIENTESUCURSAL,"
                    + "EtruckDTE.dbo.COMUNA COMUNA, EtruckDTE.dbo.PESAJE PESAJE, "
                    + "EtruckDTE.dbo.PESAJEDOCUMENTO PESAJEDOCUMENTO, "
                    + "EtruckDTE.dbo.PESAJEDOCUMENTOCAMPOS PESAJEDOCUMENTOCAMPOS, "
                    + "EtruckDTE.dbo.PESAJEDOCUMENTODETALLE PESAJEDOCUMENTODETALLE, "
                    + "EtruckDTE.dbo.PRODUCTO PRODUCTO, "
                    + "EtruckDTE.dbo.TRANSPORTISTA TRANSPORTISTA, "
                    + "EtruckDTE.dbo.CHOFER CHOFER "
                    + "WHERE COMUNA.ComCod = CLIENTESUCURSAL.CliSucComCod "
                    + "AND CLIENTESUCURSAL.CliID = CLIENTE.CliID "
                    + "AND PESAJEDOCUMENTO.PDcDstCliSucCod = CLIENTESUCURSAL.CliSucCod "
                    + "AND CLIENTE.CliID = PESAJEDOCUMENTO.PDcDstCliID "
                    + "AND PESAJEDOCUMENTO.PesBseCod = PESAJEDOCUMENTODETALLE.PesBseCod "
                    + "AND PESAJEDOCUMENTO.PesNro = PESAJEDOCUMENTODETALLE.PesNro "
                    + "AND PESAJEDOCUMENTO.PDcID = PESAJEDOCUMENTODETALLE.PDcID "
                    + "AND PESAJEDOCUMENTODETALLE.PesBseCod = PESAJEDOCUMENTOCAMPOS.PesBseCod "
                    + "AND PESAJEDOCUMENTODETALLE.PesNro = PESAJEDOCUMENTOCAMPOS.PesNro "
                    + "AND PESAJEDOCUMENTODETALLE.PDcID = PESAJEDOCUMENTOCAMPOS.PDcID "
                    + "AND PESAJEDOCUMENTO.PesNro = PESAJE.PesNro AND PESAJE.PesBseCod = PESAJEDOCUMENTOCAMPOS.PesBseCod "
                    + "AND PESAJE.PesCicPesCod = CICLOPESAJE.CicPesCod AND PESAJE.PesTraID = TRANSPORTISTA.TraID "
                    + "AND PESAJEDOCUMENTODETALLE.PDcProCod = PRODUCTO.ProCod AND ((PESAJEDOCUMENTOCAMPOS.PDcCamCod='CLOTE') "
                    + "AND PESAJE.PesBseCod=" + numero_bascula + ") and CAST(PESAJE.PesComFec AS date)>=CAST(DATEADD(day, -1, GETDATE()) AS date) and CAST(PESAJE.PesComFec AS date) <= CAST(DATEADD(day, +1, GETDATE()) AS date) ;";
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL);

            System.out.println("[" + fecha_log() + "]==INICIO RESCATE DATOS SQL SERVER PARA ACTUALIZAR REGISTROS DE AYER Y HOY===");
            while (rs.next()) {
                HashMap<String, String> row = new HashMap<>();
                row.put("bascula", rs.getString(1).trim());
                row.put("lote", rs.getString(2).trim());
                row.put("fecha", rs.getString(3).trim());
                row.put("fecha_hora", rs.getString(4).trim());
                row.put("guia", rs.getString(5).trim());
                row.put("patente", rs.getString(6).trim());
                row.put("transportista", rs.getString(7).trim());
                row.put("destino", rs.getString(8).trim());
                row.put("producto", rs.getString(9).trim());
                row.put("bruto", rs.getString(10).trim());
                row.put("tara", rs.getString(11).trim());
                row.put("neto", rs.getString(12).trim());
                row.put("numero_pesaje", rs.getString(13).trim());
                row.put("rut_chofer", "sin dato");
                row.put("nombre_chofer", "sin dato ");
                row.put("estado_guia", rs.getString(14).trim());
                data.add(row);
            }
            System.out.println("[" + fecha_log() + "]===TERMINO RESCATE DATOS SQL SERVER PARA ACTUALIZAR REGISTROS DE AYER Y HOY====");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
        return data;

    }

}
