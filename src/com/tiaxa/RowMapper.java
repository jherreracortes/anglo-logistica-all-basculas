package com.tiaxa;
import java.sql.ResultSet;

public interface RowMapper<T>
{
	public T mapRow(ResultSet rs, int row) throws Exception;
}
