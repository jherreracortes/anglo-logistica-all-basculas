package com.tiaxa;

import static com.tiaxa.SQLServer_Anglo.getData;
import static com.tiaxa.SQLServer_Anglo.getDataUpdate;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.ResultSet;
import org.postgresql.ds.PGPoolingDataSource;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Main {

    static final PGPoolingDataSource pool = new PGPoolingDataSource();
    static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault());

    public static void main(String[] args)
            throws SQLException, ParseException, Exception {

        String url = System.getProperty("CFG_FILE");
        Properties prop = getPlatformConfig(new File(url));
        setupDataBase(prop);
        ArrayList<Basculas> basculas_config = getBasculasConfig();

        for (Basculas bascula : basculas_config) {
            runSummary(bascula);
        }

    }

    public static boolean isValidDate(String proc_date) {
        try {

            sdf.setLenient(false);
            sdf.parse(proc_date);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static ArrayList<Basculas> getBasculasConfig() {

        String sql = "select numero_bascula, url_coneccion, max_id, limit_rows,descripcion,origen,id  from config.config_basculas order by id asc ;";

        RowMapper<Basculas> mapper = (ResultSet rs, int row) -> {
            Basculas bas = new Basculas();
            bas.numero_bascula = rs.getInt(1);
            bas.url_coneccion = rs.getString(2);
            bas.max_id = rs.getInt(3);
            bas.limit_rows = rs.getInt(4);
            bas.descripcion = rs.getString(5);
            bas.origen = rs.getString(6);
            bas.id = rs.getInt(7);

            return bas;
        };

        return JDBCUtilities.getResultSetGeneric(pool, mapper, sql);
    }

    public static void runSummary(Basculas bas) throws Exception {
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] Numero De Bascula    : " + bas.numero_bascula);
        System.out.println("[" + fecha_log() + "] URL Coneccion   : " + bas.url_coneccion);
        System.out.println("[" + fecha_log() + "] MAX ID  : " + bas.max_id);
        System.out.println("[" + fecha_log() + "] Limit Rows : " + bas.limit_rows);
        System.out.println("[" + fecha_log() + "] ===============================");
        ArrayList<HashMap<String, String>> data_basculas;
        ArrayList<HashMap<String, String>> data_basculas_update;
        data_basculas = getData(bas.url_coneccion, bas.numero_bascula, bas.max_id);
        data_basculas_update=getDataUpdate(bas.url_coneccion, bas.numero_bascula);
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] ========GUARDANDO REGISTROS EN POSTGRESQL=============");
        guardaBasculas(pool, data_basculas, bas.origen);
        guardaBasculas(pool, data_basculas_update, bas.origen);
        System.out.println("[" + fecha_log() + "] ========TERMINO DE GUARDADO DE REGISTROS EN POSTGRESQL=============");
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] ========ACTUALIZACION DE MAX ID==========");
        updateMaxId(pool, bas.numero_bascula, bas.id, bas.origen);
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] DATA RESCATADA DE SQL SERVER ANGLO");

        data_basculas.forEach((jj) -> {
            System.err.println(jj);
        });
      
        System.out.println("[" + fecha_log() + "] ===============================");
        System.out.println("[" + fecha_log() + "] DATA RESCATADA DE SQL SERVER ANGLO DIA ANTERIOR + DIA ACTUAL");
        data_basculas_update.forEach((jj) -> {
            System.err.println(jj);
        });
        System.out.println("[" + fecha_log() + "]===============================");
       
    }

    public static void setupDataBase(Properties props) {
        pool.setApplicationName(props.getProperty("TAG", "NO-NAME"));
        pool.setServerName(props.getProperty("DB_HOST"));
        pool.setDatabaseName(props.getProperty("DB_NAME"));
        pool.setUser(props.getProperty("DB_USER"));
        pool.setPortNumber(Integer.parseInt(props.getProperty("DB_PORT")));
        pool.setPassword(props.getProperty("DB_PASSWORD"));
        pool.setMaxConnections(1);
    }

    public static Properties getPlatformConfig(File confFile) {

        Properties properties = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream(confFile);
            properties.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return properties;
    }

    public static void updateMaxId(PGPoolingDataSource pool, Integer numero_bascula, Integer id, String origen) {
        Connection c = null;
        PreparedStatement stmt = null;

        try {
            c = pool.getConnection();
            c.setAutoCommit(false);
            String sql = "UPDATE config.config_basculas SET max_id=(select COALESCE(max(numero_pesaje),0) from work.data_basculas where bascula=? and origen=?) where numero_bascula=? and id=?;";

            stmt = c.prepareStatement(sql);

            stmt.setInt(1, numero_bascula);
            stmt.setString(2, origen);
            stmt.setInt(3, numero_bascula);
            stmt.setInt(4, id);
            stmt.executeUpdate();

            c.commit();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(-1);
        }

    }

    public static void guardaBasculas(PGPoolingDataSource pool, ArrayList<HashMap<String, String>> data_pronostico, String origen) {
        Connection c = null;
        PreparedStatement stmt = null;

        try {
            c = pool.getConnection();
            c.setAutoCommit(false);
            String sql = "INSERT INTO work.data_basculas "
                    + " (numero_pesaje, bascula, origen, lote, fecha, fecha_hora, guia, patente, transportista, destino, producto, bruto_kg, tara_kg, neto_kg,nombre_chofer,rut_chofer,estado_guia) "
                    + " VALUES(?, ?, ?, ?,(to_timestamp(?,'YYYY-MM-DD HH24:MI:SS.0'))::DATE, TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS.0'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)  ON CONFLICT (numero_pesaje, bascula,origen,guia) "
                    + " DO UPDATE SET lote = EXCLUDED.lote , fecha = EXCLUDED.fecha ,  fecha_hora = EXCLUDED.fecha_hora ,patente = EXCLUDED.patente,"
                    + " transportista = EXCLUDED.transportista , destino = EXCLUDED.destino , producto = EXCLUDED.producto , bruto_kg = EXCLUDED.bruto_kg, "
                    + " tara_kg = EXCLUDED.tara_kg , neto_kg = EXCLUDED.neto_kg , nombre_chofer = EXCLUDED.nombre_chofer , rut_chofer = EXCLUDED.rut_chofer , estado_guia = EXCLUDED.estado_guia  ;";

            stmt = c.prepareStatement(sql);
            for (HashMap<String, String> data : data_pronostico) {
                stmt.setInt(1, Integer.parseInt(data.get("numero_pesaje")));
                stmt.setInt(2, Integer.parseInt(data.get("bascula")));
                stmt.setString(3, origen);
                stmt.setString(4, data.get("lote"));
                stmt.setString(5, data.get("fecha"));
                stmt.setString(6, data.get("fecha_hora"));
                stmt.setInt(7, Integer.parseInt(data.get("guia")));
                stmt.setString(8, data.get("patente"));
                stmt.setString(9, data.get("transportista"));
                stmt.setString(10, data.get("destino"));
                stmt.setString(11, data.get("producto"));
                stmt.setInt(12, Integer.parseInt(data.get("bruto")));
                stmt.setInt(13, Integer.parseInt(data.get("tara")));
                stmt.setInt(14, Integer.parseInt(data.get("neto")));
                stmt.setString(15, data.get("nombre_chofer"));
                stmt.setString(16, data.get("rut_chofer"));
                stmt.setString(17, data.get("estado_guia"));
                stmt.executeUpdate();
            }
            c.commit();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(-1);
        }

    }
    
    public static String fecha_log() {
		Date date = new Date();
		String finalDate = new String();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		calendario.add(5, 0);
		String formatFecha;
                formatFecha = "yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat sdfl = new SimpleDateFormat(formatFecha);
		finalDate = sdfl.format(calendario.getTime());

		return finalDate;
	}

}
