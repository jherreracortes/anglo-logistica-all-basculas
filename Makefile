include ~/devenv.mk
include ../runenv.mk


OBJ=dist/resIBO.jar
EXE=./resIBO.sh
EXE2=./dropEventDaily.sh
CFG=resIBO.cfg
make: 
	ant main -DGIT_REVISION="${GIT_REVISION}"
clean:
	ant clean
install: clean make 
	sudo install -m 600 -o apptiaxa -g apptiaxa $(OBJ) $(BASE)/$(PROJECT)/bin
	sudo install -m 700 -o apptiaxa -g apptiaxa $(EXE) $(BASE)/$(PROJECT)/bin
	sudo install -m 700 -o apptiaxa -g apptiaxa $(EXE2) $(BASE)/$(PROJECT)/bin
	sudo install -m 600 -o apptiaxa -g apptiaxa $(CFG) $(BASE)/$(PROJECT)/cfg


