 CREATE TABLE config.config_basculas (
	id serial NOT NULL,
	numero_bascula int4 NOT NULL,
	url_coneccion varchar(255) NOT NULL,
	descripcion varchar(160) NOT NULL,
	max_id int4 NOT NULL,
	limit_rows int4 NOT NULL,
	origen varchar(155) NULL,
	CONSTRAINT config_basculas_pkey PRIMARY KEY (id)
);

INSERT INTO config.config_basculas (id, numero_bascula, url_coneccion, descripcion, max_id, limit_rows, origen) VALUES(1, 7, 'jdbc:sqlserver://172.16.1.25:93;databaseName=EtruckDTE;user=concentrado_read;password=c4nc2ntr1d4', 'consulta a las tortolas 1 para la bascula 7', 249422, 1000, 'TORTOLAS 1 AACLLTOD0097');
INSERT INTO config.config_basculas (id, numero_bascula, url_coneccion, descripcion, max_id, limit_rows, origen) VALUES(2, 18, 'jdbc:sqlserver://172.16.1.25:94;databaseName=EtruckDTE;user=concentrado_read;password=c4nc2ntr1d4', 'consulta a las tortolas 2 para la bascula 18', 238868, 1000, 'TORTOLAS 2 AACLLTOD0132');
INSERT INTO config.config_basculas (id, numero_bascula, url_coneccion, descripcion, max_id, limit_rows, origen) VALUES(3, 5, 'jdbc:sqlserver://172.16.1.25:95;databaseName=EtruckDTE;user=concentrado_read;password=c4nc2ntr1d4', 'consulta a soldado para la bascula 5', 90332, 1000, 'SOLDADO AACLSOLD0003');
INSERT INTO config.config_basculas (id, numero_bascula, url_coneccion, descripcion, max_id, limit_rows, origen) VALUES(4, 2, 'jdbc:sqlserver://172.16.1.25:96;databaseName=EtruckDTE;user=concentrado_read;password=c4nc2ntr1d4', 'consulta a chagres para la bascula 2', 488961, 1000, 'CHAGRES AACLCGED0006');


